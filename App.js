import React from 'react';
import { Text, View } from 'react-native';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import { HomeScreens } from './src/screens/HomeScreens';
import { SettingsScreen } from './src/screens/SettingsScreen';
import { AddPaymentScreen } from './src/screens/AddPaymentScreen';



const TabNavigator = createBottomTabNavigator({
  Resume: HomeScreens,
  Add: AddPaymentScreen,
  Settings: SettingsScreen,
},{
  tabBarVisible: false,
}); 

export default createAppContainer(TabNavigator);
