import React from 'react';

import { Text, 
         View,
         Image,
         StyleSheet,
         SafeAreaView,
    } from 'react-native';

function Header(props){
    return (
      <View>
        <SafeAreaView>
          <View style={style.header_container}>
            <Image 
              source={require('../../../assets/images/icon.png')} 
              style={style.header_logo}
            />
            <View style={style.header_right}>
              {props.children}
            </View>
          </View>
        </SafeAreaView>
      </View>
    );
}


const style = StyleSheet.create({
    header_container: {
      paddingVertical: 10,
      paddingHorizontal: 10,
      flexDirection: 'row',
    },
    header_logo: {
      width: 30,
      height: 30,
      resizeMode: 'contain',
    },
    header_title: {
      fontSize: 80,
    },
    header_right: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-end',
    }
});
export default Header;