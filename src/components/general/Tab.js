import React from 'react';
import { Text, View } from 'react-native';
import { createBottomTabNavigator, createAppContainer, TabNavigator } from 'react-navigation';
import { HomeScreens } from '../screens/HomeScreens';
import { SettingsScreen } from '../screens/SettingsScreen';
import { AddPaymentScreen } from '../screens/AddPaymentScreen';

export const Tab = TabNavigator({
	Resume: HomeScreens,
	Add: AddPaymentScreen,
	Settings: SettingsScreen,
});