import React, { Component } from 'react';

import { Text, 
         View,
         Image,
         StyleSheet,
         Picker,
         SafeAreaView,
    } from 'react-native';

import Resumetable from './Resumetable';

class Resume extends Component{
	renderEmpty = () => <Text>No hay resultados de resumen</Text>

	
	constructor(props){
	    super(props);
	    this.state = {
			period: this.props.periods[0].period,
			periods: this.props.periods,
			resume: this.props.periods[0].info,
		};
	  }

	render(){
		console.log(this.props.periods);
		return ( 
	      <View>
	          <Text style={style.titles}>Periodos</Text>
		      <Picker
		          selectedValue={this.state.period}
		          onValueChange={(itemValue, itemIndex) => this.onChangePeriod(itemValue)} >
		          {
		            this.props.periods.map( (item, key)=>{ 
		             return <Picker.Item label={item.period} value={item.period} key={key} />
		            }) 
		           }
		          </Picker>
		         <Resumetable resume={this.state.resume} />
	      </View>
	    );
	}

	onChangePeriod(_period){
		this.setState({period: _period});
		this.state.periods.map((period) => {
		   if(_period == period.period){
		   	this.setState({resume: period.info});
		   }
		});
	}
}


const style = StyleSheet.create({
	title: {
	  fontSize: 20,
	},
	titles: {
		fontSize: 20,
		fontWeight: 'bold',
	},
});
export default Resume;


