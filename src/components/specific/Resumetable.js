import React, { Component } from 'react';

import { Text, 
         View,
         Image,
         StyleSheet,
         Picker,
         SafeAreaView, 
    } from 'react-native';

import { DataTable } from 'react-native-paper';


class Resumetable extends Component{
	renderEmpty = () => <Text>No hay resultados de resumen</Text>

	
	constructor(props){
	    super(props);
	 //    this.state = {
		// 	period: '',
		// 	periods: '',
		// };


		// this.state.periods = this.props.periods;
	  }

	render(){ 

 
		return (
	      <View>
	         <DataTable>
		        <DataTable.Header>
		          <DataTable.Title>Categoria</DataTable.Title>
		          <DataTable.Title numeric>Valor</DataTable.Title>
		        </DataTable.Header>
		        {
			       	
			    	this.props.resume.map((element) => {
				      return (
				         <DataTable.Row key={element.id}>
		                  <DataTable.Cell>{element.category}</DataTable.Cell>
		                  <DataTable.Cell numeric>{element.price}
		                  </DataTable.Cell>
		                </DataTable.Row>
				      )
				    })
		   		}

		      </DataTable>
	      </View>
	    );
	}
}


const style = StyleSheet.create({
	title: {
	  fontSize: 20,
	},
	titles: {
		fontSize: 20,
		fontWeight: 'bold',
	},
});
export default Resumetable;


