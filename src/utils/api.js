const baseApi = 'http://thedeveloper.co/api/v1/financApp2.php?key=938a3a1d76e589a2c253685247dd1dc6&action=';
const serviceGetAllParameteres = baseApi + 'getResumeWithParams';


class Api{

	async getAllParameteres(){
		const query = await fetch(`${serviceGetAllParameteres}`);
		const {resume} = await query.json();
		return resume;
	}

}

export default new Api();