import React, { Component } from 'react';

import { Text, 
         View,
         StyleSheet,
         ScrollView,
         ActivityIndicator,
    } from 'react-native';

import Header from '../components/general/Header';
import Resume from '../components/specific/Resume';
import Api from '../utils/api'
import {getNetStatus} from '../utils/helper'

export class HomeScreens extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props){
    super(props);
    this.state = {
        period: '',
        loading: true,
        periods: [],
        text_connection: '',
    };
    this.parameters = {};
  }

  async componentDidMount(){
    let text_connection = 'No conectado';
    if(getNetStatus()){
      text_connection = 'conectado';
    }
    let periods = await Api.getAllParameteres();
    this.setState({
      loading: false,
      periods: periods,
      text_connection: text_connection,
    });
  }

  render() {
    return (
      <ScrollView stickyHeaderIndices={[0]}>
        <Header>
          <Text style={style.title}>Resume things</Text>
        </Header>
        <View  style={style.content}>
          <Text>{this.state.text_connection}</Text>
         {this.state.loading?
            <ActivityIndicator size="large" style={style.loading} />
          :
            <Resume periods={this.state.periods} />
         }
        </View>
      </ScrollView>
    );
  }
}


const style = StyleSheet.create({
  container: {
    /*flex: 1, 
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding:20,*/
  },
  loading: {
    color: "#ff0050",
    flex: 1, 
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  content: {
    padding: 20,
  },
  title: {
    fontSize: 20,
  }
});
