import React, { Component } from 'react';

import { Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  Picker,
  AsyncStorage,
  KeyboardAvoidingView,
  Button,
		 } from 'react-native';
import Header from '../components/general/Header';
import KeyboardShift from '../components/general/KeyboardShift';
import RadioGroup from 'react-native-radio-buttons-group';
import DatePicker from 'react-native-datepicker';
import { TextInputMask } from 'react-native-masked-text';
 

 
export class AddPaymentScreen extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props){
    super(props);
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year

    this.parameters = {
          price: 0,
          radioType: [
              {
                  label: 'Egreso',
                  value: 'Egreso'
              },
              {
                  label: 'Ingreso',
                  value: "Ingreso",
              },
          ],

          categories: [
              {
                  label: 'Mercado',
                  value: 'Mercado'
              },
              {
                  label: 'ComidaEnCalle',
                  value: 'ComidaEnCalle',
              },
              {
                  label: 'Gasolina',
                  value: 'Gasolina',
              },
          ],
          class: [
              {
                  label: 'Ninguno',
                  value: 'Ninguno'
              },
              {
                  label: 'Prestamo',
                  value: 'Prestamo',
              },
          ],
          loanResponsable: [
              {
                  label: 'Ninguno',
                  value: 'Ninguno'
              },
              {
                  label: 'Paola',
                  value: 'Paola'
              },
              {
                  label: 'Diego',
                  value: 'Diego',
              }, 
          ],
      };

      this.state = {
        date:year+"-"+month+"-"+date,
        movementType:'',
        category: this.parameters.categories[0].label,
        price: 0, 
        description: '',
        class: this.parameters.class[0].label,
        loanResponsable: this.parameters.loanResponsable[0].label,
      };
  }

  render() {
    return (
      		<ScrollView stickyHeaderIndices={[0]}>
		        <Header style={style.header}>
		          <Text style={style.title}>Add Payment</Text>
		        </Header>
		        <View>
			        <View style={style.content}>

			        	<Text style={style.titles}>Precio</Text>
				          <TextInputMask
					        type={'money'}
					        options={{
					          precision: 0,
					          separator: ',',
					          delimiter: '.',
					          unit: 'COP$ ',
					          suffixUnit: ''
					        }}
					        value={this.state.price}
					        onChangeText={text => {
					          this.setState({
					            price: text
					          })
					        }}
					      />

					      <View style = {style.line} />

					      <Text style={style.titles}>Descripcion</Text>
				          <TextInput
				            style={{height: 40, borderColor: 'gray', borderWidth: 1}}
				            onChangeText={(text) => this.setState({description: text})} 
				            value={this.state.description}
				          />

				          <View style = {style.line} />

				        <Text style={style.titles}>Tipo de Movimiento</Text>
			        	<RadioGroup radioButtons={this.parameters.radioType} 
			        	  style={style.radio}
			              onPress={this.eventMovementRadio} 
			             />
			             <View style = {style.line} />

				          <Text style={style.titles}>Seleccione una fecha</Text>
					        <DatePicker
				          style={{width: 200, justifyContent: 'center'}}
				          date={this.state.date}
				          mode="date"
				          placeholder="Seleccione una fecha"
				          format="YYYY-MM-DD" 
				          confirmBtnText="Confirmar"
				          cancelBtnText="Cancelar"
				          customStyles={{
				            dateIcon: {
				              position: 'absolute',
				              left: 0,
				              top: 4, 
				              marginLeft: 0
				            },
				            dateInput: {
				              marginLeft: 36
				            }
				          }}
				          onDateChange={(date) => {this.state.date = date}}
				          />
				        <View style = {style.line} />

				        <Text style={style.titles}>Categoria</Text>
				          <Picker
				          selectedValue={this.state.category}
				          onValueChange={(itemValue, itemIndex) => this.setState({category: itemValue})} >
				          {
				            this.parameters.categories.map( (item, key)=>{ 
				             return <Picker.Item label={item.label} value={item.value} key={item.value} />
				            }) 
				           }
				          </Picker>
				          <View style = {style.line} />

				          <Text style={style.titles}>Prestamo?</Text>
				          <Picker
				          selectedValue={this.state.class}
				          onValueChange={(itemValue, itemIndex) => this.setState({class: itemValue})} >
				          {
				            this.parameters.class.map( (item, key)=>{ 
				             return <Picker.Item label={item.label} value={item.value} key={item.value} />
				            }) 
				           }
				          </Picker>
				          <View style = {style.line} />

				          <Text style={style.titles}>Responsable</Text>
				          <Picker
				          selectedValue={this.state.loanResponsable}
				          onValueChange={(itemValue, itemIndex) => this.setState({loanResponsable: itemValue})} >
				          {
				            this.parameters.loanResponsable.map( (item, key)=>{ 
				             return <Picker.Item label={item.label} value={item.value} key={item.value} />
				            }) 
				           }
				          </Picker>
				          <View style = {style.line} />

				          <Button
				            onPress={this.saveData} 
				            title="Guardar"
				            color="#008B00"
				            accessibilityLabel="Guardar"
				            />
			        </View>
			    </View>    
			</ScrollView>         
      
    );
  }

  saveData(){
  	
    //let user ={ name:'diego'};
    //AsyncStorage.setItem('user',JSON.stringify(user));
  }

  getData = async () => {
    // try{
    //   let user = await AsyncStorage.getItem('user');
    //   user = JSON.parse(user);
    //   alert(user.name);
    // }catch(error){
    //   alert(error);
    // }
 
    // alert('diego ' + JSON.stringify(this.registry));  
    alert('diego ' + JSON.stringify(this.state));  

  }

  //EVENTOS PARA ELEMENTOS
  eventMovementRadio = (data) => {
      for (var i = 0; i < data.length; i++) {
        let element = data[i];
        if(element.selected){
          this.state.movementType = element.value; 
        }
      }
    }
}


const style = StyleSheet.create({
  container: {
    /*flex: 1, 
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding:20,*/
  },
  header: {
  	backgroundColor: '#ffffff',
  },
  title: {
    fontSize: 20,
  },
  titles: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  content: {
  	padding: 20,
  },
  radio: {
  	justifyContent: 'flex-start',
  	alignItems: 'flex-start',
  },
  line:{
        borderWidth: 0.5,
        borderColor:'black',
        margin:10,
   }
});



